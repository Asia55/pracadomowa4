package pl.codementors.mages;

import pl.codementors.mages.menu.*;

import java.util.Scanner;

/**
 * Created by psysiu on 6/28/17.
 */
public class Manager {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String command;

        CoresManager coresManager = new CoresManager();
        MagesManager magesManager = new MagesManager();
        SpellBooksManager spellBooksManager = new SpellBooksManager();
        SpellsManager spellsManager = new SpellsManager();
        WandsManager wandsManager = new WandsManager();
        WoodsManager woodsManager = new WoodsManager();

        boolean run = true;

        while (run) {
            System.out.print("command: ");
            command = scanner.next();

            switch (command) {
                case "cores": {
                    coresManager.manage(scanner);
                    break;
                }
                case "mages": {
                    magesManager.manage(scanner);
                    break;
                }
                case "spell_books": {
                    spellBooksManager.manage(scanner);
                    break;
                }
                case "spells": {
                    spellsManager.manage(scanner);
                    break;
                }
                case "wands": {
                    wandsManager.manage(scanner);
                    break;
                }
                case "woods": {
                    woodsManager.manage(scanner);
                    break;
                }
                case "quit": {
                    run = false;
                    break;
                }
            }
        }
    }

}
