package pl.codementors.mages.database;

import org.apache.commons.dbcp2.BasicDataSource;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by psysiu on 6/28/17.
 */
public class ConnectionFactory {

    private final static String URL = "jdbc:mysql://localhost:3306/mage_admin";

    private final static String USER = "mage_admin";

    private final static String PASSWORD = "m4g34dm1n";

    private final static String PARAMS = "?useUnicode=true"
            + "&useJDBCCompliantTimezoneShift=true"
            + "&useLegacyDatetimeCode=false"
            + "&serverTimezone=UTC";

    private static BasicDataSource ds;

//    public static Connection createConnection() throws SQLException {
//        Connection connection = DriverManager.getConnection(URL + PARAMS, USER, PASSWORD);
//        return connection;
//    }

    public static Connection createConnection() throws SQLException {
        if (ds == null) {
            ds = new BasicDataSource();
            ds.setUsername(USER);
            ds.setPassword(PASSWORD);
            ds.setUrl(URL + PARAMS);
        }
        Connection connection = ds.getConnection();
        return connection;
    }
}
