package pl.codementors.mages.menu;


import pl.codementors.mages.dao.BaseDAO;

import java.util.List;
import java.util.Scanner;

/**
 * Created by psysiu on 6/29/17.
 */
public abstract class BaseManager<T, D extends BaseDAO<T>> {

    protected D dao;

    public void manage(Scanner scanner) {
        System.out.print("command: ");
        String command = scanner.next();
        switch (command) {
            case "add": {
                add(scanner);
                break;
            }
            case "list": {
                list();
                break;
            }
            case "find": {
                find(scanner);
                break;
            }
            case "delete": {
                delete(scanner);
                break;
            }
            case "update": {
                update(scanner);
                break;
            }
        }
    }

    protected abstract T parseNew(Scanner scanner);

    protected abstract void copyId(T from, T to);

    private void update(Scanner scanner) {
        System.out.print("id: ");
        int id = scanner.nextInt();
        T value = dao.find(id);
        System.out.println(value);
        T newValue = parseNew(scanner);
        copyId(value, newValue);
        dao.update(newValue);
    };

    private void delete(Scanner scanner) {
        System.out.print("id: ");
        int id = scanner.nextInt();
        dao.delete(id);
    }

    private void find(Scanner scanner) {
        System.out.print("id: ");
        int id = scanner.nextInt();
        T value = dao.find(id);
        System.out.println(value);
    }

    private void list() {
        List<T> values = dao.findALl();
        for (T v : values) {
            System.out.println(v);
        }
    }

    private void add(Scanner scanner) {
        T newValue = parseNew(scanner);
        dao.insert(newValue);
    }

}
