package pl.codementors.mages.menu;

import pl.codementors.mages.dao.SpellBooksDAO;
import pl.codementors.mages.model.SpellBooks;

import java.util.Scanner;

/**
 * Created by student on 09.07.17.
 */
public class SpellBooksManager extends BaseManager<SpellBooks, SpellBooksDAO> {

    public SpellBooksManager() {
        dao = new SpellBooksDAO();
    }

    @Override
    protected SpellBooks parseNew(Scanner scanner) {
        return null;
    }

    @Override
    protected void copyId(SpellBooks from, SpellBooks to) {
        to.setId(from.getId());
    }
}
