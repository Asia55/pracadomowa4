package pl.codementors.mages.menu;

import pl.codementors.mages.dao.SpellsDAO;
import pl.codementors.mages.model.Spells;

import java.util.Scanner;

/**
 * Created by student on 09.07.17.
 */
public class SpellsManager extends BaseManager<Spells, SpellsDAO> {

    public SpellsManager() {
        dao = new SpellsDAO();
    }

    @Override
    protected Spells parseNew(Scanner scanner) {
        return null;
    }

    @Override
    protected void copyId(Spells from, Spells to) {
        to.setId(from.getId());
    }
}
