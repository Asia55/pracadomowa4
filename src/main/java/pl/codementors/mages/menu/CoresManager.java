package pl.codementors.mages.menu;

import pl.codementors.mages.dao.CoresDAO;
import pl.codementors.mages.model.Cores;

import java.util.Scanner;

/**
 * Created by student on 09.07.17.
 */
public class CoresManager extends BaseManager<Cores, CoresDAO> {

    public CoresManager() {
        dao = new CoresDAO();
    }

    @Override
    protected Cores parseNew(Scanner scanner) {
        return null;
    }

    @Override
    protected void copyId(Cores from, Cores to) {
        to.setId(from.getId());
    }
}
