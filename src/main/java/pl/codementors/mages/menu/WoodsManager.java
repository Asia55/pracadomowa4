package pl.codementors.mages.menu;

import pl.codementors.mages.dao.WoodsDAO;
import pl.codementors.mages.model.Woods;

import java.util.Scanner;

/**
 * Created by student on 09.07.17.
 */
public class WoodsManager extends BaseManager<Woods, WoodsDAO> {

    public WoodsManager() {
        dao = new WoodsDAO();
    }

    @Override
    protected Woods parseNew(Scanner scanner) {
        return null;
    }

    @Override
    protected void copyId(Woods from, Woods to) {
        to.setId(from.getId());
    }
}
