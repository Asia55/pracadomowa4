package pl.codementors.mages.menu;

import pl.codementors.mages.dao.WandsDAO;
import pl.codementors.mages.model.Wands;

import java.util.Scanner;

/**
 * Created by student on 09.07.17.
 */
public class WandsManager extends BaseManager<Wands, WandsDAO> {

    public WandsManager() {
        dao = new WandsDAO();
    }

    @Override
    protected Wands parseNew(Scanner scanner) {
        return null;
    }

    @Override
    protected void copyId(Wands from, Wands to) {
        to.setId(from.getId());
    }
}
