package pl.codementors.mages.menu;

import pl.codementors.mages.dao.MagesDAO;
import pl.codementors.mages.model.Mages;

import java.util.Scanner;

/**
 * Created by student on 09.07.17.
 */
public class MagesManager extends BaseManager<Mages, MagesDAO> {

    public MagesManager() {
        dao = new MagesDAO();
    }

    @Override
    protected Mages parseNew(Scanner scanner) {
        return null;
    }

    @Override
    protected void copyId(Mages from, Mages to) {
        to.setId(from.getId());
    }
}
