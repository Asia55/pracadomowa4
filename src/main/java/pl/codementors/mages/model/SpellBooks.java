package pl.codementors.mages.model;

import lombok.*;

import java.util.Date;

/**
 * Created by student on 09.07.17.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class SpellBooks {

    private int id;
    private String title;
    private String author;
    private Date publishDate;

    public SpellBooks(String title, String author, Date publishDate) {
        this.title = title;
        this.author = author;
        this.publishDate = publishDate;
    }

}
