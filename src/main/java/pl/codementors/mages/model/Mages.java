package pl.codementors.mages.model;

import lombok.*;

/**
 * Created by student on 05.07.17.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class Mages {

    private int id;
    private String name;
    private Wands wand;
    private Mages supervisior;

    public Mages(String name, Wands wand, Mages supervisior) {
        this.name = name;
        this.wand = wand;
        this.supervisior = supervisior;
    }
}
