package pl.codementors.mages.model;

import lombok.*;

import java.util.Date;

/**
 * Created by student on 05.07.17.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class Wands {

    private int id;
    private Woods wood;
    private Cores core;
    private Date productionDate;

    public Wands(Woods wood, Cores core, Date productionDate) {
        this.wood = wood;
        this.core = core;
        this.productionDate = productionDate;
    }
}
