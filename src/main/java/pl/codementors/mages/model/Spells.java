package pl.codementors.mages.model;

import lombok.*;

/**
 * Created by student on 05.07.17.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class Spells {

    private int id;
    private String incantation;

    public Spells(String incantation) {
        this.incantation = incantation;
    }
}
