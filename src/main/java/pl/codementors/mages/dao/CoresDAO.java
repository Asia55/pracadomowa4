package pl.codementors.mages.dao;

import pl.codementors.mages.model.Cores;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by student on 09.07.17.
 */
public class CoresDAO extends BaseDAO<Cores> {

    private String[] columns = {"name", "power", "consistency"};

    @Override
    public String getTableName() {
        return "cores";
    }

    @Override
    public Cores parseValue(ResultSet result) throws SQLException {
        int id = result.getInt(1);
        String name = result.getString(2);
        int power = result.getInt(3);
        int consistency = result.getInt(4);

        return new Cores(id, name, power, consistency);
    }

    @Override
    public String[] getColumns() {
        return columns;
    }

    @Override
    public Object[] getColumnsValues(Cores value) {
        Object[] values = {value.getName(), value.getPower(), value.getConsistency()};
        return values;
    }

    @Override
    public int getPrimaryKeyValue(Cores value) {
        return value.getId();
    }

    public Cores create(String name, int power, int consistency) {
        return new Cores(name, power, consistency);
    }
}
