package pl.codementors.mages.dao;

import pl.codementors.mages.model.Mages;
import pl.codementors.mages.model.Wands;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by student on 09.07.17.
 */
public class MagesDAO extends BaseDAO<Mages> {

    private String[] columns = {"name", "wand", "supervisor"};

    private WandsDAO wandsDAO = new WandsDAO();
    private MagesDAO magesDAO = new MagesDAO();

    @Override
    public String getTableName() {
        return "mages";
    }

    @Override
    public Mages parseValue(ResultSet result) throws SQLException {
        int id = result.getInt(1);
        String name = result.getString(2);
        int wandsId = result.getInt(3);
        Wands wands = wandsDAO.find(wandsId);
        int supervisorId = result.getInt(4);
        Mages supervisor = magesDAO.find(supervisorId);
        return new Mages(id, name, wands, supervisor);
    }

    @Override
    public String[] getColumns() {
        return columns;
    }

    @Override
    public Object[] getColumnsValues(Mages value) {
        Object[] values = {value.getName(), value.getWand().getId(), value.getSupervisior().getId()};
        return values;
    }

    @Override
    public int getPrimaryKeyValue(Mages value) {
        return value.getId();
    }

    public Mages create(String name, Wands wand, Mages supervisor) {
        return new Mages(name, wand, supervisor);
    }
}
