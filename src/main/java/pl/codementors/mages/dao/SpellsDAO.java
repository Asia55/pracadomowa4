package pl.codementors.mages.dao;

import pl.codementors.mages.model.Spells;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by student on 09.07.17.
 */
public class SpellsDAO extends BaseDAO<Spells> {

    private String[] columns = {"incantation"};

    @Override
    public String getTableName() {
        return "spells";
    }

    @Override
    public Spells parseValue(ResultSet result) throws SQLException {
        int id = result.getInt(1);
        String incantation = result.getString(2);
        return new Spells(id, incantation);
    }

    @Override
    public String[] getColumns() {
        return columns;
    }

    @Override
    public Object[] getColumnsValues(Spells value) {
        Object[] values = {value.getIncantation()};
        return values;
    }

    @Override
    public int getPrimaryKeyValue(Spells value) {
        return value.getId();
    }

    public Spells create(String incantation) {
        return new Spells(incantation);
    }
}
