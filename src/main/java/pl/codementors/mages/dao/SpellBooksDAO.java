package pl.codementors.mages.dao;

import pl.codementors.mages.model.SpellBooks;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * Created by student on 09.07.17.
 */
public class SpellBooksDAO extends BaseDAO<SpellBooks> {

    private String[] columns = {"title", "author", "publish_date"};

    @Override
    public String getTableName() {
        return "spell_books";
    }

    @Override
    public SpellBooks parseValue(ResultSet result) throws SQLException {
        int id = result.getInt(1);
        String title = result.getString(2);
        String author = result.getString(3);
        java.sql.Date sqlDate = result.getDate(4);
        Date publishDate = new Date(sqlDate.getTime());
        return new SpellBooks(id, title, author, publishDate);
    }

    @Override
    public String[] getColumns() {
        return columns;
    }

    @Override
    public Object[] getColumnsValues(SpellBooks value) {
        Object[] values = {value.getTitle(), value.getAuthor(), value.getPublishDate()};
        return values;
    }

    @Override
    public int getPrimaryKeyValue(SpellBooks value) {
        return value.getId();
    }

    public SpellBooks create(String title, String author, Date publishDate) {
        return new SpellBooks(title, author, publishDate);
    }
}
