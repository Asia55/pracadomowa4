package pl.codementors.mages.dao;

import pl.codementors.mages.model.Cores;
import pl.codementors.mages.model.Wands;
import pl.codementors.mages.model.Woods;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * Created by student on 09.07.17.
 */
public class WandsDAO extends BaseDAO<Wands> {

    private String[] columns = {"wood", "core", "production_date"};

    private WoodsDAO woodsDAO = new WoodsDAO();
    private CoresDAO coresDAO = new CoresDAO();

    @Override
    public String getTableName() {
        return "wands";
    }

    @Override
    public Wands parseValue(ResultSet result) throws SQLException {
        int id = result.getInt(1);
        int woodsId = result.getInt(2);
        Woods woods = woodsDAO.find(woodsId);
        int coresId = result.getInt(3);
        Cores cores = coresDAO.find(coresId);
        java.sql.Date sqlDate = result.getDate(4);
        Date productionDate = new Date(sqlDate.getTime());
        return new Wands(id, woods, cores, productionDate);
    }

    @Override
    public String[] getColumns() {
        return columns;
    }

    @Override
    public Object[] getColumnsValues(Wands value) {
        Object[] values = {value.getWood().getId(), value.getCore().getId(), value.getProductionDate()};
        return values;
    }

    @Override
    public int getPrimaryKeyValue(Wands value) {
        return value.getId();
    }

    public Wands create(Woods wood, Cores core, Date productionDate) {
        return new Wands(wood, core, productionDate);
    }
}
