package pl.codementors.mages.dao;

import pl.codementors.mages.model.Woods;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by student on 09.07.17.
 */
public class WoodsDAO extends BaseDAO<Woods> {

    private String[] columns = {"name", "toughness"};

    @Override
    public String getTableName() {
        return "woods";
    }

    @Override
    public Woods parseValue(ResultSet result) throws SQLException {
        int id = result.getInt(1);
        String name = result.getString(2);
        int toughness = result.getInt(3);
        return new Woods(id, name, toughness);
    }

    @Override
    public String[] getColumns() {
        return columns;
    }

    @Override
    public Object[] getColumnsValues(Woods value) {
        Object[] values = {value.getName(), value.getToughness()};
        return values;
    }

    @Override
    public int getPrimaryKeyValue(Woods value) {
        return value.getId();
    }

    public Woods create(String name, int toughness) {
        return new Woods(name, toughness);
    }
}
